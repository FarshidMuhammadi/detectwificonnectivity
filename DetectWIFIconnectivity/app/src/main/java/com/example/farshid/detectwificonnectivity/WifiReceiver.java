package com.example.farshid.detectwificonnectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.widget.Toast;

public class WifiReceiver extends BroadcastReceiver {
    public WifiReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        if(networkInfo != null && networkInfo.isConnected()) {

            WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            String ssidName = wifiInfo.getSSID();

            Toast.makeText(context,"Your are connected to the "+ssidName +"wifi",Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(context,"Your wifi connection has been lost",Toast.LENGTH_LONG).show();
        }
    }
}
